import QtQuick 2.7
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3
import Lomiri.DownloadManager 1.2
import 'components'
import 'app.js' as App

Page {
    id: fullView

    property string src
    property string id

    property var tags: []
    property bool tagsCreated: false
    property int itemOpacity: 0

    header: PageHeader { visible: false }

    Image {
        id: img
        width: fullView.width
        height: fullView.height
        source: src
        fillMode: Image.PreserveAspectCrop

        // pinch to zoom
        PinchArea {
            id: pnch
            property double newWidth: img.width
            property double newHeight: img.height
            property double lastScale

            MouseArea {
                anchors.fill: parent
                drag.target: img
                drag.axis: Drag.XAndYAxis
                drag.minimumX: -(pnch.newWidth - fullView.width) / 2
                drag.maximumX: (pnch.newWidth - fullView.width) / 2
                drag.minimumY: -(pnch.newHeight - fullView.height) / 2
                drag.maximumY: (pnch.newHeight - fullView.height) / 2
            }

            anchors.fill: img
            pinch.target: img
            pinch.maximumScale: 5.0
            pinch.minimumScale: 1.0
            onPinchFinished: pnch.lastScale = img.scale
            onPinchUpdated: {
                newWidth = img.width * img.scale
                newHeight = img.height * img.scale
                if(img.scale < lastScale) {
                    img.x = fullView.x
                    img.y = fullView.y
                }
            }
        }

        onProgressChanged: {
            if(progress === 1.0) spinner.running = false
            itemOpacity = 1
        }
    }

    ActivityIndicator {
        id: spinner
        running: true
        anchors.centerIn: parent
    }

    // grid of tags for the image
    Flow {
        id: tagFlow

        property int buttonOpacity: 0

        width: innerRow.width
        height: childrenRect.height
        anchors.centerIn: parent
        spacing: units.gu(1)
    }

    // buttons for 'back', 'tags' and 'save'
    RowLayout {
        id: actions
        anchors {
            bottom: parent.bottom
            bottomMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        width: parent.width - units.gu(8)

        RowLayout {
            id: innerRow
            Layout.minimumWidth: units.gu(30)
            Layout.maximumWidth: units.gu(80)
            Layout.alignment: Qt.AlignHCenter

            Rectangle {
                id: backBtn
                Layout.alignment: Qt.AlignLeft
                opacity: itemOpacity
                width: units.gu(5)
                height: width
                color: root.solidButtons ? theme.palette.normal.background : Qt.rgba(0,0,0,0.3)
                radius: units.gu(1)
                border {
                    width: units.dp(1)
                    color: root.solidButtons ? theme.palette.normal.backgroundText : Qt.rgba(1,1,1,0.3)
                }
                Icon {
                    name: 'back'
                    color: root.solidButtons ? theme.palette.normal.backgroundText : '#fff'
                    width: parent.width / 2
                    height: width
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: stack.pop()
                }
                Behavior on opacity {
                    NumberAnimation { duration: LomiriAnimation.BriskDuration; easing: LomiriAnimation.StandardEasing }
                }
            }

            Rectangle {
                id: showTags
                Layout.alignment: Qt.AlignHCenter
                opacity: itemOpacity
                width: units.gu(5)
                height: width
                color: root.solidButtons ? theme.palette.normal.background : Qt.rgba(0,0,0,0.3)
                radius: units.gu(1)
                border {
                    width: units.dp(1)
                    color: root.solidButtons ? theme.palette.normal.backgroundText : Qt.rgba(1,1,1,0.3)
                }
                Icon {
                    name: 'tag'
                    color: root.solidButtons ? theme.palette.normal.backgroundText : '#fff'
                    width: parent.width / 2
                    height: width
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(!tagsCreated) {
                            for(var i = 0; i < tags.length; i++) {
                                App.createSpriteObjects(tags[i])

                            }
                            tagsCreated = true
                        }
                        tagFlow.buttonOpacity === 0 ? tagFlow.buttonOpacity = 1 : tagFlow.buttonOpacity = 0
                    }
                }
                Behavior on opacity {
                    NumberAnimation { duration: LomiriAnimation.BriskDuration; easing: LomiriAnimation.StandardEasing }
                }
            }

            Rectangle {
                id: saveBtn
                Layout.alignment: Qt.AlignRight
                opacity: itemOpacity
                width: units.gu(5)
                height: width
                color: root.solidButtons ? theme.palette.normal.background : Qt.rgba(0,0,0,0.3)
                radius: units.gu(1)
                border {
                    width: units.dp(1)
                    color: root.solidButtons ? theme.palette.normal.backgroundText : Qt.rgba(1,1,1,0.3)
                }
                Icon {
                    name: 'save'
                    color: root.solidButtons ? theme.palette.normal.backgroundText : '#fff'
                    width: parent.width / 2
                    height: width
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: downloader.download(src)
                }
                Behavior on opacity {
                    NumberAnimation { duration: LomiriAnimation.BriskDuration; easing: LomiriAnimation.StandardEasing }
                }
            }
        }
    }

    SingleDownload {
        id: downloader
        onFinished: stack.push(Qt.resolvedUrl('PeerPicker.qml'), { pathToImage: path })
    }

    Component.onCompleted: App.fetchInfo(id)
}
