import QtQuick 2.7
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3
import 'components'

Page {
    header: PageHeader {
        title: i18n.tr('How To Set As A Background')
    }

    Flickable {
        width: parent.width
        height: parent.height - parent.header.height
        contentHeight: col.height
        anchors.top: parent.header.bottom

        ColumnLayout {
            id: col
            width: parent.width - units.gu(4)
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: units.gu(2)
            spacing: units.gu(2)

            HelpItem { txt: '1. Click the download button on your chosen image.' }
            HelpItem { txt: '2. Choose a destination for the image, File Manager -> Downloads for example.' }
            HelpItem { txt: '3. In the Downloads folder click the check mark at the top to confirm the download.' }
            HelpItem { txt: '4. Open the System Settings app and select Background & Appearance.' }
            HelpItem { txt: '5. Scroll down and click on Custom, then click the Add an image button.' }
            HelpItem { txt: '6. Select the app that you saved the image in.' }
            HelpItem { txt: '7. Select your image and click the check mark at the top of the page.' }
            HelpItem { txt: '8. In the Preview page click the check mark at the top and you are done.' }
        }
    }
}
