/*
 * Copyright (C) 2023  Alan Moir
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * backdrops is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'backdrops.alanmoir'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property bool useDarkTheme: false
    property bool portraitOnly: false
    property bool byNewest: false
    property bool includeAi: false
    property bool includeAnime: false
    property bool includePeople: false
    property bool solidButtons: false

    property string query
    property bool isError: false
    property string errMsg: "Something has gone wrong"
    property int currentPage: 0
    property int totalImages
    property int lastPage

    onUseDarkThemeChanged: theme.name = root.useDarkTheme ? 'Lomiri.Components.Themes.SuruDark' : 'Lomiri.Components.Themes.Ambiance'

    Settings {
        id: settings
        property alias useDarkTheme: root.useDarkTheme
        property alias portraitOnly: root.portraitOnly
        property alias byNewest: root.byNewest
        property alias includeAi: root.includeAi
        property alias includeAnime: root.includeAnime
        property alias includePeople: root.includePeople
        property alias solidButtons: root.solidButtons
    }

    PageStack { id: stack }

    ListModel { id: imageModel }

    Component.onCompleted: {
        theme.name = root.useDarkTheme ? 'Lomiri.Components.Themes.SuruDark' : 'Lomiri.Components.Themes.Ambiance'
        stack.push(Qt.resolvedUrl('HomePage.qml'))
    }
}
