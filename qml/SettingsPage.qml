import QtQuick 2.7
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3
import 'components'

Page {
    id: settingsPage

    header: PageHeader {
        title: i18n.tr('Settings')
        subtitle: i18n.tr('Refresh the home page to apply changes.')
        trailingActionBar.actions: [
            Action {
                iconName: 'help'
                onTriggered: stack.push(Qt.resolvedUrl('HelpPage.qml'))
            }
        ]
    }

    Flickable {
        width: parent.width
        height: parent.height - parent.header.height
        contentHeight: col.height
        anchors.top: parent.header.bottom

        ColumnLayout {
            id: col
            width: parent.width

            Column {
                id: settings

                Layout.maximumWidth: units.gu(75)
                Layout.minimumWidth: units.gu(40)
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter

                ListItem {
                    height: lilTheme.height + divider.height
                    ListItemLayout {
                        id: lilTheme
                        title.text: i18n.tr('Dark Mode')
                        Switch {
                            checked: root.useDarkTheme
                            onCheckedChanged: root.useDarkTheme = checked
                        }
                    }
                }

                ListItem {
                    height: lilBtnColor.height + divider.height
                    ListItemLayout {
                        id: lilBtnColor
                        title.text: i18n.tr('Solid Button Color')
                        Switch {
                            checked: root.solidButtons
                            onCheckedChanged: root.solidButtons = checked
                        }
                    }
                }

                ListItem {
                    height: lilRatio.height + divider.height
                    ListItemLayout {
                        id: lilRatio
                        title.text: i18n.tr('Portrait Only')
                        subtitle.text: 'Filters out landscape images'
                        Switch {
                            checked: root.portraitOnly
                            onCheckedChanged: root.portraitOnly = checked
                        }
                    }
                }

                ListItem {
                    height: lilOrder.height + divider.height
                    ListItemLayout {
                        id: lilOrder
                        title.text: i18n.tr('Order by Newest')
                        subtitle.text: i18n.tr('Default is by relevance (recommended)')
                        Switch {
                            checked: root.byNewest
                            onCheckedChanged: root.byNewest = checked
                        }
                    }
                }

                ListItem {
                    height: lilAi.height + divider.height
                    ListItemLayout {
                        id: lilAi
                        title.text: i18n.tr('Include AI Art')
                        Switch {
                            checked: root.includeAi
                            onCheckedChanged: root.includeAi = checked
                        }
                    }
                }

                ListItem {
                    height: lilAnime.height + divider.height
                    ListItemLayout {
                        id: lilAnime
                        title.text: i18n.tr('Include Anime')
                        Switch {
                            checked: root.includeAnime
                            onCheckedChanged: root.includeAnime = checked
                        }
                    }
                }

                ListItem {
                    height: lilPeople.height + divider.height
                    ListItemLayout {
                        id: lilPeople
                        title.text: i18n.tr('Include People')
                        Switch {
                            checked: root.includePeople
                            onCheckedChanged: root.includePeople = checked
                        }
                    }
                }
            }

            LomiriShape {
                Layout.preferredWidth: units.gu(12)
                Layout.preferredHeight: units.gu(12)
                Layout.alignment: Qt.AlignHCenter
                Layout.topMargin: units.gu(4)
                source: Image { source: '../assets/backdrops.svg' }
                height: width
                radius: 'large'
            }

            Label {
                Layout.alignment: Qt.AlignHCenter
                text: 'backdrops'
                font.bold: true
                textSize: Label.XLarge
            }

            Column {
                id: links

                Layout.maximumWidth: units.gu(75)
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true

                topPadding: units.gu(4)
                bottomPadding: units.gu(4)

                ListItem {
                    height: lilSource.height + divider.height

                    ListItemLayout {
                        id: lilSource

                        title.text: 'Source Code'
                        subtitle.text: 'https://gitlab.com/irnbru/backdrops'
                        ProgressionSlot {}
                    }
                    onClicked: Qt.openUrlExternally('https://gitlab.com/irnbru/backdrops')
                }

                ListItem {
                    height: lilApi.height + divider.height

                    ListItemLayout {
                        id: lilApi

                        title.text: 'Wallhaven'
                        subtitle.text: 'Image provider'
                        ProgressionSlot {}
                    }
                    onClicked: Qt.openUrlExternally('https://wallhaven.cc/help/api')
                }
            }
        }
    }
}
