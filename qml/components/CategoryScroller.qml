import QtQuick 2.7
import Lomiri.Components 1.3
import '../app.js' as App

Column {
    id: categoryScroller

    width: parent.width
    spacing: units.gu(1)

    ListModel {
        id: categoriesModel
        ListElement { cat: 'abstract' }
        ListElement { cat: 'animals' }
        ListElement { cat: 'anime' }
        ListElement { cat: 'city' }
        ListElement { cat: 'clouds' }
        ListElement { cat: 'food' }
        ListElement { cat: 'games' }
        ListElement { cat: 'gradient' }
        ListElement { cat: 'landscape' }
        ListElement { cat: 'linux' }
        ListElement { cat: 'minimalism' }
        ListElement { cat: 'movie' }
        ListElement { cat: 'music' }
        ListElement { cat: 'nature' }
        ListElement { cat: 'photography' }
        ListElement { cat: 'plants' }
        ListElement { cat: 'science fiction' }
        ListElement { cat: 'sky' }
        ListElement { cat: 'space' }
        ListElement { cat: 'texture' }
        ListElement { cat: 'water' }
    }

    ListView {
        id: categoriesView
        width: parent.width
        height: units.gu(4)
        spacing: units.gu(3)
        orientation: ListView.Horizontal
        //header/footer add padding to start and end of view
        header: Rectangle { width: units.gu(2); height: units.gu(1); color: 'transparent' }
        footer: Rectangle { width: units.gu(2); height: units.gu(1); color: 'transparent' }
        model: categoriesModel
        delegate: Rectangle {
            height: parent.height
            width: lbl.width
            color: 'transparent'

            Label {
                id: lbl
                text: cat
                color: theme.palette.normal.backgroundTertiaryText
                font.bold: true
                anchors.verticalCenter: parent.verticalCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    App.fetchImages(cat)
                }
            }
        }
    }

    // thin line/separator so the gridview below doesnt just clip into nothingness
    Rectangle {
        id: separator
        width: parent.width
        height: units.dp(1)
        color: theme.palette.normal.base
    }
}
