import QtQuick 2.7
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3

Label {
    property string txt

    text: i18n.tr(txt)
    Layout.maximumWidth: units.gu(75)
    Layout.preferredWidth: parent.width
    Layout.alignment: Qt.AlignHCenter
    wrapMode: Text.WordWrap
}
