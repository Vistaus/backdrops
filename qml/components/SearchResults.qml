import QtQuick 2.7
import Lomiri.Components 1.3
import '../app.js' as App

GridView {
    id: resultsView

    // these are just to make tweaking easier
    property int firstBreakpoint: 100
    property int secondBreakpoint: 140

    anchors {
        top: parent.header.bottom
        bottom: parent.bottom
        horizontalCenter: parent.horizontalCenter
    }
    width: parent.width - units.gu(2)
    cellWidth: width < units.gu(firstBreakpoint) ? width / 3 : width < units.gu(secondBreakpoint) ? width / 4 : width / 5
    cellHeight: width > height ? cellWidth * 0.6 : cellWidth * 1.7
    clip: true
    //header/footer add padding to start and end of view
    header: Rectangle { width: parent.width; height: units.gu(1); color: 'transparent' }
    footer: Rectangle { width: parent.width; height: units.gu(1); color: 'transparent' }
    model: imageModel

    delegate: Item {
        width: resultsView.cellWidth
        height: resultsView.cellHeight

        LomiriShape {
            // need bigger margins on larger screens i think
            width: resultsView.width < units.gu(firstBreakpoint) ? parent.width - units.gu(1) : parent.width - units.gu(2)
            height: resultsView.width < units.gu(firstBreakpoint) ? parent.height - units.gu(1) : parent.height - units.gu(2)
            color: theme.palette.normal.base
            anchors.centerIn: parent
            source: Image { source: resultsView.width < units.gu(200) ? smallImage : mediumImage }
            onSourceChanged: console.log('source changed')
            sourceFillMode: LomiriShape.PreserveAspectCrop
            MouseArea {
                anchors.fill: parent
                onClicked: stack.push(Qt.resolvedUrl('../FullView.qml'), { src: largeImage, id: id })
            }
        }
    }

    onMovementEnded: {
        if(atYEnd) {
            if(!root.query) {
                App.fetchLatest()
            } else if(root.currentPage < root.lastPage) {
                App.fetchImages(root.query)
            }
        }
    }

    Component.onCompleted: App.fetchLatest()
}
